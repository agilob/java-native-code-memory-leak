set -e
javac src/main/java/net/agilob/Main.java
javac -h src/main/native/ src/main/java/net/agilob/Main.java

g++ -c -fPIC -Wl,-Bstatic -libc -Wl,-Bdynamic -I"${JAVA_HOME}/include/" -I"${JAVA_HOME}/include/linux" src/main/native/net_agilob_Main.cpp  -o src/main/resources/lib/x86/libleaky.o

g++ -shared  -fPIC -o src/main/resources/lib/x86/libleaky.so src/main/resources/lib/x86/libleaky.o -lc

podman run --memory=1g --cpus=1 -v $(pwd):/mnt -ti openjdk:21-jdk-slim-buster java -XX:NativeMemoryTracking=summary -Djava.library.path=/mnt/src/main/resources/lib/x86/ -cp /mnt/src/main/java/ net.agilob.Main
