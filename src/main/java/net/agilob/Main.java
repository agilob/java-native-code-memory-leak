package net.agilob;

import java.time.Duration;
import java.util.stream.IntStream;

public class Main {

    static {
        System.loadLibrary("leaky");
    }

    public static void main(String[] args) throws InterruptedException {
        IntStream.range(0, 32000) // 32gib
                .forEach((i) -> {
                    System.out.println("Allocating megabyte number " + i);
                    new Main().leakyCode(1);
                    try {
                        Thread.sleep(Duration.ofMillis(100));
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                });

        Thread.sleep(Duration.ofMinutes(1));
    }

    public native void leakyCode(int megabytes);
}
