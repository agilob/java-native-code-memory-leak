#include "net_agilob_Main.h"

JNIEXPORT void JNICALL Java_net_agilob_Main_leakyCode
        (JNIEnv* env, jobject thisObject, jint value) {

   const long long megabytes = value; // value is the number of megabytes to allocate

   int* myArray = new int[megabytes * 1024 * 1024 / sizeof(int)];

   for (size_t i = 0; i < megabytes * 1024 * 1024 / sizeof(int); ++i) {
      myArray[i] = static_cast<int>(i);
   }

}
